#!/bin/bash
mkdir -p /home/pi/Presentation-Logs/
LOGFILE="/home/pi/Presentation-Logs/`date +%G-%m-%d.%H-%m.Presentation.log`"
#echo $LOGFILE
main()
{
        if sha256sum -c /home/pi/Presentation.sha256sum &> $LOGFILE
        then
                echo "100 #Starting Presentation" | tee -a $LOGFILE
                vlc /home/pi/Presentation.mp4 -f -R &>> $LOGFILE
        else
                rm /home/pi/Presentation.mp4
                echo "10 #Preparing Directory" | tee -a $LOGFILE
                mkdir -p  /home/pi/Presentation-out &>> $LOGFILE
                echo "20 #Converting PDF" | tee -a $LOGFILE
                pdftoppm -jpeg -r 400 Presentation.pdf /home/pi/Presentation-out/Page &>> $LOGFILE
                echo "30 #Fixing images" | tee -a $LOGFILE
                mogrify -resize 800x600 /home/pi/Presentation-out/*.jpg &>> $LOGFILE
                echo "50 #Creating video" | tee -a $LOGFILE
                if ls Presentation-out/Page-??.jpg
                then
                        ffmpeg -start_number 1 -framerate 1/60 -i /home/pi/Presentation-out/Page-%01d.jpg -r 25 /home/pi/Presentation.mp4 -y &>> $LOGFILE
                elif ls Presentation-out/Page-?.jpg
                then
                        ffmpeg -start_number 1 -framerate 1/60 -i /home/pi/Presentation-out/Page-%01d.jpg -r 25 /home/pi/Presentation.mp4 -y &>> $LOGFILE
                fi
                echo "90 #Updating Checksum" | tee -a $LOGFILE
                sha256sum /home/pi/Presentation.pdf | tee /home/pi/Presentation.sha256sum &> $LOGFILE
                echo "100 #Starting Presentation" | tee -a $LOGFILE >> $LOGFILE
                vlc /home/pi/Presentation.mp4 -f -R &>> $LOGFILE
        fi
}

_ip()
{
        tmpfile=`mktemp`
        echo "Loopback:" > $tmpfile
        ip -j a | jq -r '.[] | select(.ifname=="lo") | .addr_info[] | select(.family=="inet") | .local' >> $tmpfile
        echo "Ethernet:" >> $tmpfile
        ip -j a | jq -r '.[] | select(.ifname=="eth0") | .addr_info[] | select(.family=="inet") | .local' >> $tmpfile
        echo "Wifi:" >> $tmpfile
        ip -j a | jq -r '.[] | select(.ifname=="wlan0") | .addr_info[] | select(.family=="inet") | .local' >> $tmpfile
        zenity --info --width 200 --height 50 --title "IP Address" --text "$(cat $tmpfile)"
        rm $tmpfile
}
main
#_ip
