<?php
include('lib.php');

$configFilePath = "Presentation.ini";
$config = parse_ini_file("$configFilePath");

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
	if(isset($_POST["fileToUpload"]))
	{
		$target_dir = "uploads/";
		$target_file = "/var/www/html/Presentation.pdf";
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
		// Check if image file is a actual image or fake image
	/*	if(isset($_POST["submit"]))
		{
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false)
			{
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			}
			else
			{
				echo "File is not an image. ";
				$uploadOk = 0;
			}
	 	}
	 */	
		// Check if file already exists
		if (file_exists($target_file))
		{
			echo "Sorry, file already exists.";
			$uploadOk = 0;
		}
		
		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 500000)
		{
			echo "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		
		// Allow certain file formats
		if($imageFileType != "pdf") //&& $imageFileType != "png"
		//&& $imageFileType != "jpeg" && $imageFileType != "gif" )
		{
			echo "Sorry, only PDF files are allowed.";
			$uploadOk = 0;
		}
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0)
		{
			echo "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
		}
		else
		{
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
			{
				echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
			}
			else
			{
				echo "Sorry, there was an error uploading your file.";
			}
		}
	}
	if(isset($_POST["videoTiming"]))
	{
		$config['Config']['timing'] = $_POST["videoTiming"];
		write_ini_file($configFilePath, $config);
		$config = parse_ini_file("$configFilePath");

	}	
}


?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Title of document</title>
</head>
<body>
	<h1>Presentation Upload</h1>
	<form action="/" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td>
					Document upload:
				</td>
				<td>
					<input type="file" name="fileToUpload" id="fileToUpload"
						accept="application/pdf"
					/>
				</td>
			</tr>
			<tr>
				<td>
					Presentation Timing (seconds):
				</td>
				<td>	
					<input type="number" id="videoTiming"
						name="videoTiming" min="5"
						max="300" value="<?php echo($config["timing"]);?>"/>
				</td>
			</tr>
		</table>
		<input type="submit" value="Upload Image" name="submit">
	</form>
</body>
</html> 
